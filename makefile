pythonpath=.venv/bin/python
celerypath=.venv/bin/celery
pippath=.venv/bin/pip

setup:

	@echo "\nCreating python virtual environment"
	python3 -m venv .venv

	@echo "\nInstalling python packages"

	$(pippath) install --upgrade pip
	$(pippath) install -r requiriments.txt

	@echo "\nInstall playwright!"	

	$(pythonpath) -m playwright install

	@echo "\nDone!"	

run_celery:
	$(celerypath) -A hercules.celery worker -P threads -E

run_flower:
	$(celerypath) -A hercules.celery flower --address=0.0.0.0