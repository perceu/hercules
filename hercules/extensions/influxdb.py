import logging
from datetime import datetime
from scrapy.exceptions import NotConfigured
from scrapy import signals
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# You can generate an API token from the "API Tokens Tab" in the UI


class PropagateHandler(logging.Handler):

    def __init__(self, level, host, token, org, bucket):
        super(PropagateHandler, self).__init__(level)
        self.host = host
        self.org = org
        self.bucket = bucket
        self.token = token

    def emit(self, record):
        token = self.token
        org = self.org
        bucket = self.bucket
        with InfluxDBClient(url=self.host, token=token, org=org) as client:
            write_api = client.write_api(write_options=SYNCHRONOUS)
            point = Point(record.levelname)\
                .field("log_message", record.getMessage())\
                .time(datetime.utcnow(), WritePrecision.NS)
            write_api.write(bucket, org, point)

class InfluxDbExtension:

    def __init__(self, level, host, token, org, bucket) -> None:
        self.level = level
        self.host = host
        self.token = token
        self.org = org
        self.bucket = bucket
    
    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        host = settings.get('INFLUX_URL')
        token = settings.get('INFLUX_TOKEN')
        org = settings.get('INFLUX_ORG')
        bucket = settings.get('INFLUX_BUCKET')
        level = settings.get('LOG_LEVEL')
        if host:
            ext = cls(level, host, token, org, bucket)
            crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
            return ext 
        else:
            return cls
    
    def spider_opened(self, spider):
        logger = logging.getLogger(spider.name)
        logger.info('adicionado influx log')
        logger.addHandler(PropagateHandler(self.level, self.host, self.token, self.org, self.bucket))