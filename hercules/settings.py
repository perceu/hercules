from decouple import config
# Scrapy settings for hercules project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'hercules'

SPIDER_MODULES = ['hercules.spiders']

NEWSPIDER_MODULE = 'hercules.spiders'

#Prometheus
PROMETHEUS_PUSHGATEWAY = config('PROMETHEUS_PUSHGATEWAY', default='localhost:9091')
STATS_CLASS = 'scrapy_prometheus.PrometheusStatsCollector'

PROMETHEUS_METRIC_PREFIX = 'hercules' # default
PROMETHEUS_PUSH_METHOD = 'POST'
PROMETHEUS_JOB = 'scrapy' 
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Hercules-Robot'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Splash Configs
SPLASH_URL = config('SPLASH_URL', default='http://localhost:8050')
DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# SELENIUM DRIVER 
# suport: firefox|chrome
SELENIUM_DRIVER_NAME = config('SELENIUM_DRIVER_NAME', default='chrome')

# SELENIUM 
# Rodar no selenium Grid:
# SELENIUM_COMMAND_EXECUTOR = 'http://localhost:4444/wd/hub'
# Rodar no selenium:
# SELENIUM_COMMAND_EXECUTOR = False
SELENIUM_COMMAND_EXECUTOR = config('SELENIUM_COMMAND_EXECUTOR', default='http://localhost:4444/wd/hub')

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html

SPIDER_MIDDLEWARES = {
    'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
}

DOWNLOAD_HANDLERS = {
    "http": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
    "https": "scrapy_playwright.handler.ScrapyPlaywrightDownloadHandler",
}

TWISTED_REACTOR = "twisted.internet.asyncioreactor.AsyncioSelectorReactor"

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html

EXTENSIONS = {
    'hercules.extensions.influxdb.InfluxDbExtension': 500,
}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html

ITEM_PIPELINES = {
   'hercules.pipelines.mongoDB.MongoPipeline': 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

LOG_LEVEL = config('LOG_LEVEL', default="DEBUG")

INFLUX_URL = config('INFLUX_URL', default="")
INFLUX_TOKEN = config('INFLUX_TOKEN', default='')
INFLUX_ORG = config('INFLUX_ORG', default="")
INFLUX_BUCKET = config('INFLUX_BUCKET', default="")

MONGO_URI = config('MONGO_URI', default="mongodb://mongo:mongo@127.0.0.1")
MONGO_DATABASE = config('MONGO_DATABASE', default="response_items")