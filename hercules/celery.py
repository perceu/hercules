from celery import Celery
from decouple import config
from multiprocessing import Process
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

app = Celery('hercules', broker=config('CELERY_BROKER_URL', default='amqp://guest:guest@localhost:5672'))

@app.task(name='run')
def run(spyder_to_run):
    def execute_crawling(spyder):
        process = CrawlerProcess(get_project_settings())#same way can be done for Crawlrunner
        process.crawl(spyder)
        process.start()
        return True
    p = Process(target=execute_crawling, args=(spyder_to_run,))
    p.start()
    p.join()
    return True