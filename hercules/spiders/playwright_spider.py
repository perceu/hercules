import scrapy
from prometheus_client import Counter

class PlaywrightSpider(scrapy.Spider):

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, self.parse,
                meta={"playwright": True, "playwright_include_page": True}
            )

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(PlaywrightSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def spider_closed(self, spider):
        registry = spider.crawler.stats.get_registry(spider)
        stats = spider.crawler.stats.get_stats()

        elapsed_time_captcha_solve = stats['elapsed_time_captcha_solve']
        c = Counter('elapsed_time_captcha_solve', 'Description of counter', registry=registry)
        c.inc(elapsed_time_captcha_solve)

        elapsed_time_seconds = stats['elapsed_time_seconds']
        c = Counter('elapsed_time_seconds', 'Description of counter', registry=registry)
        c.inc(elapsed_time_seconds)

        item_scraped_count = stats['item_scraped_count']
        c = Counter('item_scraped_count', 'Description of counter', registry=registry)
        c.inc(item_scraped_count)

        spider.logger.info('Spider closed: %s', spider.name)