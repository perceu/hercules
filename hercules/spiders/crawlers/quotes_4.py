import scrapy
from random import randint
from time import sleep
from timeit import default_timer as timer
import urllib.parse
import scrapy
from hercules.spiders.selenium_spider import SeleniumSpider

class Quotes4(SeleniumSpider):
    name = 'quotes4'
    start_urls = [
        'http://quotes.toscrape.com/',
    ]
    
    def parse(self, _):
        self.logger.info('Start Parse Quotes Selenium')
        responses = []
        selector = scrapy.Selector(text=self.driver.page_source.encode('utf-8'))
        responses.extend(self.get_messages(selector))
        next_page_url = selector.xpath('//li[@class="next"]/a/@href').extract_first()
        while next_page_url is not None:
            start_captcha = timer()
            for i in range(randint(2,5)):
                sleep(1)
            end_captcha = timer()
            
            self.crawler.stats.inc_value('elapsed_time_captcha_solve', (end_captcha-start_captcha))

            next_page = urllib.parse.urljoin(self.driver.current_url, next_page_url)
            self.logger.info('To next page Quotes Selenium')
            self.driver.get(next_page)

            selector = scrapy.Selector(text=self.driver.page_source.encode('utf-8'))

            responses.extend(self.get_messages(selector))
            next_page_url = selector.xpath('//li[@class="next"]/a/@href').extract_first()
        for response in responses:
            yield response
        self.logger.info('End Parse Quotes Selenium')

    def get_messages(self, selector):
        messages = []
        for quote in selector.xpath('//div[@class="quote"]'):
            if range(randint(0,1)):
                messages.append({
                    'text': quote.xpath('./span[@class="text"]/text()').extract_first(),
                    'author': quote.xpath('.//small[@class="author"]/text()').extract_first(),
                    'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
                })
            else:
                continue
        return messages