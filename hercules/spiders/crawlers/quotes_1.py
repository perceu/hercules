from random import randint
from time import sleep
from timeit import default_timer as timer
import scrapy

from hercules.spiders.requests_spider import RequestSpider


class Quotes1(RequestSpider):
    name = 'quotes1'
    start_urls = [
        'http://quotes.toscrape.com/',
    ]

    def parse(self, response):
        self.logger.info('Start Parse Quotes Default')
        for quote in response.xpath('//div[@class="quote"]'):
            if range(randint(0,1)):
                yield {
                    'text': quote.xpath('./span[@class="text"]/text()').extract_first(),
                    'author': quote.xpath('.//small[@class="author"]/text()').extract_first(),
                    'tags': quote.xpath('.//div[@class="tags"]/a[@class="tag"]/text()').extract()
                }
            else:
                continue

        start_captcha = timer()
        for i in range(randint(2,5)):
            sleep(1)
        end_captcha = timer()

        self.crawler.stats.inc_value('elapsed_time_captcha_solve', (end_captcha-start_captcha))
        next_page_url = response.xpath('//li[@class="next"]/a/@href').extract_first()
        if next_page_url is not None:
            self.logger.info('To next page Quotes Default')
            yield scrapy.Request(response.urljoin(next_page_url))

        self.logger.info('End page Quotes Default')