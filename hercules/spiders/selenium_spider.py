import scrapy
from selenium import webdriver
from prometheus_client import Counter
from hercules.settings import SELENIUM_COMMAND_EXECUTOR, SELENIUM_DRIVER_NAME
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager



class SeleniumSpider(scrapy.Spider):

    def __init__(self, name=None, **kwargs):
        super().__init__(name, **kwargs)
        self.driver = self.get_driver()

    @classmethod
    def get_driver(cls):
        options = {}
        if SELENIUM_DRIVER_NAME == 'firefox':
            options = webdriver.FirefoxOptions()
        elif SELENIUM_DRIVER_NAME == 'chrome':
            options = webdriver.ChromeOptions()
        else:
            raise Exception('SELENIUM_DRIVER_NAME: Driver Name not Found in settings')

        if SELENIUM_COMMAND_EXECUTOR:
            return webdriver.Remote(
                command_executor=SELENIUM_COMMAND_EXECUTOR,
                options=options
            )
        else:
            if SELENIUM_DRIVER_NAME == 'firefox':
                return webdriver.Firefox(executable_path=GeckoDriverManager().install(), options=options)
            elif SELENIUM_DRIVER_NAME == 'chrome':
                return webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=options)
            else:
                raise Exception('SELENIUM_DRIVER_NAME: Driver Name not Found in settings')


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(SeleniumSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_closed, signal=scrapy.signals.spider_closed)
        return spider

    def start_requests(self):
        self.driver.get(self.start_urls[0])
        return super().start_requests()

    def spider_closed(self, spider):
        self.driver.quit()
        registry = spider.crawler.stats.get_registry(spider)
        stats = spider.crawler.stats.get_stats()

        elapsed_time_captcha_solve = stats['elapsed_time_captcha_solve']
        c = Counter('elapsed_time_captcha_solve', 'Description of counter', registry=registry)
        c.inc(elapsed_time_captcha_solve)

        elapsed_time_seconds = stats['elapsed_time_seconds']
        c = Counter('elapsed_time_seconds', 'Description of counter', registry=registry)
        c.inc(elapsed_time_seconds)

        item_scraped_count = stats['item_scraped_count']
        c = Counter('item_scraped_count', 'Description of counter', registry=registry)
        c.inc(item_scraped_count)

        spider.logger.info('Spider closed: %s', spider.name)