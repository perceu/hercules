# Hercules rodando tudo via scrapy


## para instalar:
```
    make
```

## para rodar as dependencias:
```
    docker-compose up -d 
```

# Dependencias:
 - Mongo `#Armazenamento das respostas`
 - rabbitmq `#broken`
 - flower `#para monitorar `
 - splash `#navegador commandline `
 - selenium-hub `#gerenciador do selenium grid`
 - firefox `#selenium`
 - chrome `#selenium`

## para rodar na mão:
script rodando playwrite, requests, splash, selenium_grid and selenium.

```
source .venv/bin/activate

scrapy crawl quotes1 -o response1.json
scrapy crawl quotes2 -o response2.json
scrapy crawl quotes3 -o response3.json
scrapy crawl quotes4 -o response4.json

```

## para rodar via celery:
script rodando playwrite, requests, splash, selenium_grid and selenium.

```
make run_celery
```

em outro terminal:

```
source .venv/bin/activate
python examples/taskrun.py

```

