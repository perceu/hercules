FROM python:3

WORKDIR /usr/src/app

RUN apt-get update

RUN pip install --upgrade pip

COPY ./makefile makefile
COPY ./hercules hercules
COPY ./requiriments.txt requiriments.txt
COPY ./scrapy.cfg scrapy.cfg

RUN make

