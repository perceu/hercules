from celery import Celery
from decouple import config

app = Celery('teste', broker=config('CELERY_BROKER_URL', default='amqp://guest:guest@localhost:5672'))

app.send_task('run', ('quotes1',))
app.send_task('run', ('quotes2',))
app.send_task('run', ('quotes3',))
app.send_task('run', ('quotes4',))

